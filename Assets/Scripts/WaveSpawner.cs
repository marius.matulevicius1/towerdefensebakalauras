﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WaveSpawner : MonoBehaviour
{
    public GameObject enemyPrefab1;
    public GameObject enemyPrefab2;
    public GameObject road1;
    public GameObject road2;
    public GameObject road3;
    public GameObject destination;


    public void LaunchNextWave(int numberOfEnemies, int numberOfStrongEnemies, int RoadToTravel)
    {
        StartCoroutine(SpawnWaiter(numberOfStrongEnemies, numberOfEnemies, RoadToTravel));
    }

    private IEnumerator SpawnWaiter(int numberofStrongEnemies, int numberOfEnemies, int raodToTravel)
    {
        for (int i = 0; i < numberOfEnemies; i++)
        {
            StartCoroutine( spawnEnemy(enemyPrefab1, 1, raodToTravel));
            yield return new WaitForSeconds(0.5f);
        }
        for (int i = 0; i < numberofStrongEnemies; i++)
        {
            StartCoroutine( spawnEnemy(enemyPrefab2, 1, raodToTravel));
            yield return new WaitForSeconds(0.5f);
        }
    }

    public IEnumerator LaunchScoutingWave(int numberofStrongEnemies, int numberOfEnemies)
    {
        for (int i = 1; i < 4; i++)
        {
            for (int j = 0; j < numberOfEnemies; j++)
            {
                StartCoroutine(spawnEnemy(enemyPrefab1, 1, i));
                yield return new WaitForSeconds(0.5f);
            }
            for (int k = 0; k < numberofStrongEnemies; k++)
            {
                StartCoroutine(spawnEnemy(enemyPrefab2, 1, i));
                yield return new WaitForSeconds(0.5f);
            }
        }
    }

    // Start is called before the first frame update
    private IEnumerator spawnEnemy(GameObject enemy, int nrOfEnemies, int roadToTravel)
    {
        for (int i = 0; i < nrOfEnemies; i++)
        {
            GameObject priesas = Instantiate(enemy);
            enemycontroller sc = priesas.GetComponent<enemycontroller>();
            enemy en = priesas.GetComponent<enemy>();

            sc.road1Target = road1;
            sc.road2Target = road2;
            sc.road3Target = road3;
            sc.destination = destination;
            sc.roadToTravel = roadToTravel;
            en.StartHealth = en.StartHealth * (WaveSpawningDecider.WaveNumber / 10 + 1);

            yield return new WaitForSeconds(0.1f);
        }
    }
}
