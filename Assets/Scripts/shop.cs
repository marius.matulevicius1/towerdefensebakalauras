﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class shop : MonoBehaviour
{
    buildManager buildManager;
    public turrentBlueprint standardTurrent;
    public turrentBlueprint missileLauncher;

    private void Start()
    {
        buildManager = buildManager.instance;
    }

    // Start is called before the first frame update
    public void SelectStandardTurrent()
    {
        Debug.Log("Standard Purchased");
        buildManager.SelectTurrentToBuild(standardTurrent);
    }

    public void SelectRocketTurrent()
    {
        Debug.Log("Rocekt Purchased Purchased");
        buildManager.SelectTurrentToBuild(missileLauncher);
    }
}
