﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class node : MonoBehaviour
{

    public Color hoverColor;
    public Color notEnoughMoneyColor;
    public Vector3 positionOffset;

    public GameObject turrent;

    private Renderer rend;
    private Color startColor;

    buildManager buildmanager;

    private void OnMouseDown()
    {
        if (EventSystem.current.IsPointerOverGameObject())
        {
            return;
        }
        if (!buildmanager.canBuild)
        {
            return;
        }

        if (turrent != null)
        {
            Debug.Log("Soriukas");
            return;
        }
        buildmanager.BuildTurrentOn(this);
    }

    private void Start()
    {
        rend = GetComponent<Renderer>();
        startColor = rend.material.color;
        buildmanager = buildManager.instance;

    }

    public Vector3 GetBuiltPossition()
    {
        return transform.position + positionOffset;
    }

    private void OnMouseEnter()
    {
        if (EventSystem.current.IsPointerOverGameObject())
            return;

        if (!buildmanager.canBuild)        
            return;
        if (buildmanager.hasMoney)
        {
            rend.material.color = hoverColor;
        } else
        {
            rend.material.color = notEnoughMoneyColor;
        }

        if (turrent != null)
        {
            Debug.Log("Soriukas");
            return;
        }
    }

    private void OnMouseExit()
    {
        rend.material.color = startColor;
    }
}
