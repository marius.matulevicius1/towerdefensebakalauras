﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Turrent : MonoBehaviour
{
    public Transform target;
    public float range = 15;

    public Transform partToRotate;
    public Transform firePart;

    public GameObject bullet;

    public float fireRate = 1f;
    private float fireCountdown = 0f;

    public bool canFire = false;
    void setTarget()
    {
        GameObject[] enemies = GameObject.FindGameObjectsWithTag("Enemy");

        float shortestDistance = Mathf.Infinity;
        GameObject nearestEnemy = null;

        foreach (GameObject enemy in enemies)
        {
            float tempDistance = Vector3.Distance(enemy.transform.position, transform.position);
            if (tempDistance < shortestDistance)
            {
                shortestDistance = tempDistance;
                nearestEnemy = enemy;
            }
        }

        if (nearestEnemy != null && shortestDistance <= range)
        {
            target = nearestEnemy.transform;
        }
        else
        {
            target = null;
        }
    }

    void Start()
    {
        InvokeRepeating("setTarget", 0f, 0.5f);
    }

    // Update is called once per frame
    void Update()
    {
        if (target == null)
        {
            return;
        }

        Vector3 direction = target.transform.position - transform.position;
        Quaternion lookRotation = Quaternion.LookRotation(direction);
        Vector3 rotation = Quaternion.Lerp(partToRotate.rotation, lookRotation, Time.deltaTime * 10f).eulerAngles;

        partToRotate.rotation = Quaternion.Euler(0f, rotation.y, 0f);
        if (fireCountdown <= 0)
        {
            shoot();
            fireCountdown = 1 / fireRate;
        }

        fireCountdown -= Time.deltaTime;

    }

    void shoot()
    {
        GameObject bulletGO = (GameObject)Instantiate(bullet, firePart.position, firePart.rotation);
        bullet bl = bulletGO.GetComponent<bullet>();

        if (bl != null)
        {
            bl.Seek(target);
        }
    }

    private void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(transform.position, range);
    }
}
