﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class buildManager : MonoBehaviour
{
    private turrentBlueprint turrentToBuild;

    public static buildManager instance;
    public GameObject standardTurrentPrefab;
    public GameObject laserTurrentPrefab;
    public GameObject RocketTurrentPrefab;

    private void Awake()
    {
        if (instance != null)
        {
            Debug.Log("more than one build manager");
            return;
        }
        instance = this;
    }

    public bool canBuild { get {  return turrentToBuild != null; } }
    public bool hasMoney { get { return PlayerStats.Money >= turrentToBuild.cost; } }


    public void BuildTurrentOn( node nodes)
    {
        if (PlayerStats.Money < turrentToBuild.cost)
        {
            Debug.Log("not enough money");
            return;
        }
        PlayerStats.Money -= turrentToBuild.cost;
        GameObject turrent = (GameObject) Instantiate(turrentToBuild.prefab, nodes.GetBuiltPossition(), Quaternion.identity);
        nodes.turrent = turrent;
    }

    public void SelectTurrentToBuild(turrentBlueprint turrent)
    {
        turrentToBuild = turrent;
    }
}
