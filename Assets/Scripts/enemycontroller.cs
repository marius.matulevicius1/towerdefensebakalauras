﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class enemycontroller : MonoBehaviour
{

    public GameObject destination;
    public GameObject road1Target;
    public GameObject road2Target;
    public GameObject road3Target;

    public int roadToTravel;
    public int originalPathToTravel;
    public NavMeshAgent agent;

    // Start is called before the first frame update
    void Start()
    {
        originalPathToTravel = roadToTravel;
        agent.SetDestination(destination.transform.position);
    }

    void getRoadObjects()
    {
        road1Target = GameManager.road1Object;
        road2Target = GameManager.road2Object;
        road3Target = GameManager.road3Object;
    }

    // Update is called once per frame
    void Update()
    {
        //Debug.Log(agent.remainingDistance);
        if (roadToTravel == 1)
        {

            agent.SetDestination(road1Target.transform.position);

            if (agent.remainingDistance < 5f)
            {
                roadToTravel = 4;
                Debug.Log("Road 4");

            }
        }
        else if (roadToTravel == 2)
        {
            agent.SetDestination(road2Target.transform.position);

            if (agent.remainingDistance < 5f)
            {
                roadToTravel = 4;
                Debug.Log("Road 4");

            }

        }
        else if (roadToTravel == 3)
        {
            agent.SetDestination(road3Target.transform.position);

            if (agent.remainingDistance < 5f)
            {
                roadToTravel = 4;
                Debug.Log("Road 4");
            }
        }

        if (roadToTravel == 4)
        {
            agent.SetDestination(destination.transform.position);
            if (agent.remainingDistance < 2f) 
            {
                Destroy(gameObject);
                PlayerStats.Lives -= 1;
                DataCollector.AddSuccessfullEnemies(originalPathToTravel);
            }
        }

    }
}
