﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class DataAnalizer
{
    private static float WeakestPath = 0;

    public static float CalculateWeakestPath()
    {
        calculateByEnemiesPassed();
        if (WeakestPath == 0) WeakestPath = CalculateByTimePassed();
        return WeakestPath;
    }


    private static void calculateByEnemiesPassed()
    {
        float path1PassedEnemiesCount = DataCollector.path1SuccessfulEnemiesCount;
        float path2PassedEnemiesCount = DataCollector.path2SuccessfulEnemiesCount;
        float path3PassedEnemiesCount = DataCollector.path3SuccessfulEnemiesCount;

        if (path1PassedEnemiesCount == 0 && path2PassedEnemiesCount == 0 && path3PassedEnemiesCount == 0)
        {
            WeakestPath = 0;
            return;
        }
        if (path1PassedEnemiesCount > path2PassedEnemiesCount && path1PassedEnemiesCount > path3PassedEnemiesCount)
        {
            WeakestPath = 1;
        }
        if (path2PassedEnemiesCount > path1PassedEnemiesCount && path2PassedEnemiesCount > path3PassedEnemiesCount)
        {
            WeakestPath = 2;
        }
        if (path3PassedEnemiesCount > path1PassedEnemiesCount && path3PassedEnemiesCount > path2PassedEnemiesCount)
        {
            WeakestPath = 3;
        }
        if (path1PassedEnemiesCount == path2PassedEnemiesCount && path3PassedEnemiesCount < path1PassedEnemiesCount && path3PassedEnemiesCount < path2PassedEnemiesCount)
        {
            WeakestPath = Random.Range(1, 2);
        }
        if (path2PassedEnemiesCount == path3PassedEnemiesCount && path1PassedEnemiesCount < path2PassedEnemiesCount && path1PassedEnemiesCount < path3PassedEnemiesCount)
        {
            WeakestPath = Random.Range(2, 3);
        }
        if (path1PassedEnemiesCount == path3PassedEnemiesCount && path2PassedEnemiesCount < path1PassedEnemiesCount && path2PassedEnemiesCount < path3PassedEnemiesCount)
        {
            int rnd = Random.Range(1, 2);
            if (rnd == 1) WeakestPath = 1;
            if (rnd == 2) WeakestPath = 3;
        }
    }

    private static int CalculateByTimePassed()
    {
        float avg1 = DataCollector.Path1Avarege();
        float avg2 = DataCollector.Path2Avarege();
        float avg3 = DataCollector.Path3Avarege();

        if (avg1 > avg2 && avg1 > avg3) return 1;
        else
        if (avg2 > avg1 && avg2 > avg3) return 2;
        else return 3;
    }
}
