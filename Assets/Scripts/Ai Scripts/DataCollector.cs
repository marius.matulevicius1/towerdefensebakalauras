﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class DataCollector
{
    public static List<float> path1Times = new List<float>();
    public static List<float> path2Times = new List<float>();
    public static List<float> path3Times = new List<float>();

    public static float path1SuccessfulEnemiesCount = 0;
    public static float path2SuccessfulEnemiesCount = 0;
    public static float path3SuccessfulEnemiesCount = 0;

    public static void AddWaveTime(float time, int path)
    {
        if (path == 1) path1Times.Add(time);
        
        if (path == 2) path2Times.Add(time);

        if (path == 3) path3Times.Add(time);
    }

    public static void AddSuccessfullEnemies(int path)
    {
        if (path == 1) path1SuccessfulEnemiesCount++;

        if (path == 2) path2SuccessfulEnemiesCount++;

        if (path == 3) path3SuccessfulEnemiesCount++;
    }

    public static void ResetNumbersForNextWave()
    {
        path1Times.RemoveRange(0, path1Times.Count - 1);
        path2Times.RemoveRange(0, path2Times.Count - 1);
        path3Times.RemoveRange(0, path3Times.Count - 1);

        path1SuccessfulEnemiesCount = 0;
        path2SuccessfulEnemiesCount = 0;
        path3SuccessfulEnemiesCount = 0;
    }

    public static float Path1Avarege()
    {
        float sum = 0;
        for (int i = 0; i < path1Times.Count; i++)
        {
            sum += path1Times[i];
        }
        return sum / path1Times.Count;

    }
    public static float Path2Avarege()
    {
        float sum = 0;
        for (int i = 0; i < path2Times.Count; i++)
        {
            sum += path2Times[i];
        }
        return sum / path2Times.Count;
    }
    public static float Path3Avarege()
    {
        float sum = 0;
        for (int i = 0; i < path3Times.Count; i++)
        {
            sum += path3Times[i];
        }
        return sum / path3Times.Count;
    }
}
