﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WaveSpawningDecider : MonoBehaviour
{
    public static float WaveNumber = 0;
    public int numberOfEnemies = 10;
    public int numberOfStrongEnemies = 3;
    WaveSpawner spawner;

    private void Start()
    {
        spawner = GetComponent<WaveSpawner>();
    }

    public void launchNextWave()
    {
        WaveNumber++;
        numberOfEnemies = (int) calcultateNumberOfEnemies();
        numberOfStrongEnemies = (int) calcultateNumberOfStrongEnemies();
        if (WaveNumber == 1) launchScountingWave();
        else if (WaveNumber % 3 == 0) launchScountingWave();
        else launchCalculatedWave();
    }

    private double calcultateNumberOfEnemies()
    {
        double NextNumberOfEnemies = numberOfEnemies * 1.2;
        return NextNumberOfEnemies;
    }

    private double calcultateNumberOfStrongEnemies()
    {
        double NextNumberOfStrongEnemies = numberOfStrongEnemies * 1.5;
        return NextNumberOfStrongEnemies;
    }
    private void launchScountingWave()
    {
        int nrOfEnemies = (int) numberOfEnemies / 3;
        int nrOfStrongEnemies = (int)numberOfStrongEnemies / 3;
        StartCoroutine(spawner.LaunchScoutingWave(nrOfStrongEnemies, nrOfEnemies));
    }

    private void launchCalculatedWave()
    {
        int weakestPath = (int) DataAnalizer.CalculateWeakestPath();
        spawner.LaunchNextWave(numberOfEnemies,numberOfStrongEnemies, weakestPath);

    }

}
