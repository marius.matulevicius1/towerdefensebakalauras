﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class enemy : MonoBehaviour
{
    public float StartHealth;
    private float health;
    private float timeAlive = 0;

    public Image healthBar;

    // Start is called before the first frame update
    void Start()
    {
        health = StartHealth;
    }

    public void TakeDamage(float amount)
    {
        health -= amount;
        healthBar.fillAmount = health / StartHealth;
        die();
    }

    void die()
    {
        if (health <= 0)
        {
            sendData();
            Destroy(gameObject);
            PlayerStats.Money += 10;
        }
    }

    private void sendData()
    {
        DataCollector.AddWaveTime(timeAlive, gameObject.GetComponent<enemycontroller>().originalPathToTravel);
    }

    private void Update()
    {
        timeAlive += Time.deltaTime;
    }
}
