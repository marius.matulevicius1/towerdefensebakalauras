﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameOver : MonoBehaviour
{
    // Update is called once per frame
    void Update()
    {
        if (PlayerStats.Lives <= 0)
        {
            Debug.Log("GameOver");
        }
    }
}
